package com.company;

public class Main {



    public static void main(String[] args) {

        ////////////////////
        //
        //Task 1
        //
        ////////////////////

        System.out.println("________Start task 1________");

        recursion(6);
        System.out.println();

        System.out.println("________End task 1________\n\n\n");
        ///////////end Task 1////////////////


        ////////////////////
        //
        //Task 2
        //
        ////////////////////
        System.out.println("________Start task 2________");

        System.out.println("none recirsia");
        System.out.println(pow(2, 6));

        System.out.println("recirsia");
        System.out.println(powRecursia(2, 6));

        System.out.println();
        property(2, 16);
        System.out.println();
        System.out.println("________End task 2________\n\n\n");
        ///////////end Task 2////////////////



        ///////////////////
        //
        //Task 3
        //
        ////////////////////
        System.out.println("________Start task 3________");



        System.out.println("________End task 3________\n\n\n");
        ///////////end Task 3////////////////
    }



    //Task 1
    private static void recursion(int number){
        if(number != 0){
            number /= 2;
            recursion(number);
        } else return;

        System.out.print(number%2);
    }


    //Task 2
    private static int pow(int N, int e){
        int teamp = N;
        while (e != 1){
            N *= teamp;
            e--;
        }
        return N;
    }


    //Task 2
    private static int powRecursia(int N, int e){

        if(e == 1){
            return N;
        } else {
            return N * powRecursia(N, e - 1);
        }

    }

    //Task 2
    private static int step2(int number, int step){
        return step != 1 ? number * step2(number, step - 1) : number;
    }


    //Task 2
    //а^2n = (a^n)^2
    //2^16=(2^8)^2 =[(2^4)^2]^2 и т д
    private static void property(int number, int step){
        if(step != 1)
            System.out.print("("+number + "^"+step+")");

        if(step % 2 == 0){
            step /= 2;
            property(number, step);
        } else return;
    }




}
